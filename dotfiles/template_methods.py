import sys

# Imports to make the function available in templates
from packaging.version import parse as version  # noqa: F401


def python_path() -> str:
    return sys.executable

def escape_double_quotes(s: str) -> str:
    return s.replace('"', '\\"')
