#!/bin/sh

systemctl --user stop xsession.target

# Wait for services to deactivate
while [ -n "$(systemctl --user --no-legend --state=deactivating list-units)" ]
do
    sleep 0.2
done
