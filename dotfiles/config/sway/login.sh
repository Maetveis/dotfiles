#!/bin/sh

# robustness: if the previous graphical session left some
# failed units, reset them so that they don't break startup
for unit in $(systemctl --user --full --no-legend --plain --state=failed list-units | cut -f1 -d' '); do
    if [ "$(systemctl --user show -p PartOf --value)" = "graphical-session.target" ]; then
        systemctl --user reset-failed "$unit"
    fi
done

# Start the services
systemctl --user restart --wait xsession.target
