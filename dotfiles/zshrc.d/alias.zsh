# {{@@ header() @@}}

# Color ip command
alias ip='ip --color=auto'

# Dotdrop
alias dotdrop='{{@@ repo_path @@}}/dotdrop.sh'
