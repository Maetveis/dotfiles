import os
import sys
from enum import Enum
from pathlib import Path

RUNCOMS = [
    "zlogin",
    "zpreztorc",
    "zprofile",
    "zshenv",
]

FileState = Enum("FileState", ["MISSING", "DIFFERENT", "SAME"])


class NotAFileError(Exception):
    pass


def check_existing(link_path: Path, expected_target: Path) -> FileState:
    if not link_path.exists():
        return FileState.MISSING

    if not link_path.is_file():
        raise NotAFileError("Destination path already exists, and is not a file")

    if not link_path.is_symlink():
        return FileState.DIFFERENT

    if not os.readlink(link_path) != expected_target:
        return FileState.DIFFERENT

    return FileState.SAME


def backup(link_path: Path) -> None:
    dst = link_path.with_suffix(link_path.suffix + ".bak")
    print(f"backup {link_path} to {dst}", file=sys.stderr)
    link_path.replace(dst)


def main() -> int:
    runcoms_src = Path(sys.argv[1]).relative_to(Path.home())

    has_failure = False
    for runcom in RUNCOMS:
        link_path = Path.home() / ("." + runcom)
        target = Path(runcoms_src, runcom)
        try:
            state = check_existing(link_path, target)
        except Exception as e:
            print(f"Failed to create {link_path}: {e}", file=sys.stderr)
            has_failure = True
            continue

        if state == FileState.SAME:
            continue

        if state == FileState.DIFFERENT:
            backup(link_path)

        link_path.symlink_to(target)

    return 0 if has_failure else 0


if __name__ == "__main__":
    sys.exit(main())
