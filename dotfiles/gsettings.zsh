#!/usr/bin/env zsh

gnome_schema=org.gnome.desktop.interface

gsettings set $gnome_schema gtk-theme Sweet
gsettings set $gnome_schema color-scheme prefer-dark
gsettings set $gnome_schema icon-theme candy-icons
gsettings set $gnome_schema cursor-theme Sweet-cursors
gsettings set $gnome_schema font-name 'Noto Sans,  10'
