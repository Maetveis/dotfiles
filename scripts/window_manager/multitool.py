#!/usr/bin/env python3

import i3ipc
import typer
import logging

app = typer.Typer()

def _current_workspace(i3: i3ipc.Connection, workspaces: list[i3ipc.WorkspaceReply] | None) -> i3ipc.WorkspaceReply:
    if not workspaces:
        workspaces = i3.get_workspaces()
    for ws in workspaces:
        if ws.focused:
            return ws
    raise RuntimeError("No focused workspace found")


def _next_available_workspace(i3: i3ipc.Connection) -> int:
    workspaces = i3.get_workspaces()

    # ws.num can be -1 for named workspaces, but we don't care
    # we wouldn't hit it with our loop below anyway
    workspace_numbers = set([int(ws.num) for ws in workspaces])

    ws = _current_workspace(i3, workspaces)
    start = ws.num + 1 if ws.num != -1 else 1
    for i in range(start, 100):
        if i not in workspace_numbers:
            return i
    raise RuntimeError("No available workspace number found")


@app.command("workspace_new")
def workspace_new():
    """Create a new workspace on the current output and switch to it"""
    i3 = i3ipc.Connection()
    i3.command("workspace number {}".format(_next_available_workspace(i3)))


@app.command("workspace_renumber")
def workspace_renumber():
    """Renumber workspaces to be consecutive"""
    i3 = i3ipc.Connection()
    workspaces = i3.get_workspaces()
    for i, ws in enumerate(workspaces, 1):
        if ws.num == i:
            continue
        i3.command("rename workspace number {} to {}".format(ws.num, i))


@app.command("move_container_to_new_workspace")
def move_container_to_new_workspace():
    """Move the currently focused container to a new workspace"""
    i3 = i3ipc.Connection()
    i3.command("move container to workspace number {}".format(_next_available_workspace(i3)))


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app()
