#!/usr/bin/env bash
# author: deadc0de6 (https://github.com/deadc0de6)
# Copyright (c) 2017, deadc0de6
# Modifications maetveis 2023

# setup variables
args=("$@")
cur=$(cd "$(dirname "${0}")" && pwd)
opwd=$(pwd)

# pivot
cd "${cur}" || { echo "Directory \"${cur}\" doesn't exist, aborting." && exit 1; }

# shellcheck source=/dev/null
source .venv/bin/activate

# check python executable
pybin="python3"
hash ${pybin} 2>/dev/null || pybin="python"
[[ "$(${pybin} -V 2>&1)" =~ "Python 3" ]] || { echo "install Python 3" && exit 1; }

# launch dotdrop
PYTHONPATH=dotdrop:${PYTHONPATH} ${pybin} -m dotdrop.dotdrop "${args[@]}"
ret="$?"

# pivot back
cd "${opwd}" || { echo "Directory \"${opwd}\" doesn't exist, aborting." && exit 1; }

# exit with dotdrop exit code
exit ${ret}
