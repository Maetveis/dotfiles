#!/usr/bin/env sh

TERMINFO_URL="https://raw.githubusercontent.com/alacritty/alacritty/master/extra/alacritty.info"
SOPS_REL="v3.9.0"

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

if ! git -C "$BASEDIR" submodule update --init --recursive; then
    echo "Failed to fetch submodules."
    exit 1
fi

if ! python3 -m venv --clear "$BASEDIR/.venv"; then
    echo "Failed to create virtualenv!"
    exit 1
fi
# shellcheck source=/dev/null
. "$BASEDIR/.venv/bin/activate"

if ! pip3 install --upgrade -r "$BASEDIR/dotdrop/requirements.txt"; then
    echo "Installing dependencies failed. Is pip installed?"
    exit 1
fi

# Download command based on curl or fall back to wget
if command -v curl > /dev/null; then
    _get() {
        curl --fail --silent --show-error --location --output "$1" "$2"
    }
else
    _get() {
        wget --output-document="$1" "$2"
    }
fi

if ! command -v sops > /dev/null; then
    mkdir --parents ~/.local/bin
    _get ~/.local/bin/sops "https://github.com/getsops/sops/releases/download/$SOPS_REL/sops-$SOPS_REL.linux.amd64"
    chmod +x ~/.local/bin/sops
fi

"$BASEDIR/dotdrop.sh" install -f -p shell

# Install terminfo file for alacritty
_get alacritty.info "$TERMINFO_URL"
tic -xe alacritty,alacritty-direct alacritty.info
rm alacritty.info
